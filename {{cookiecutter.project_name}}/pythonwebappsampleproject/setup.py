import sys
from setuptools import setup, find_packages

from pythonwebappsampleproject.settings import *

assert sys.version_info >= MINIMUM_PYTHON_VERSION

setup(
    name="python webapp sample project",
    version=VERSION,
    description="python-webapp-sample-project",
    author="Terminal Labs",
    author_email="solutions@terminallabs.com",
    license="mit",
    packages=find_packages(),
    zip_safe=False,
    include_package_data=True,
    install_requires=[
        "setuptools",
        "utilities-package@git+https://gitlab.com/terminallabs/utilitiespackage/utilities-package.git@master#egg=utilitiespackage&subdirectory=utilitiespackage",
    ],
    entry_points="""
      [console_scripts]
      pythonwebappsampleproject=pythonwebappsampleproject.__main__:main
  """,
)
